<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    //
    protected $fillable = [
        'nome', 'modelo', 'preco', 'preco_fipe', 'ativo', 'descricao', 'montadora_id'
    ];

    protected $guarded = ['id', 'created_at', 'update_at'];


    function montadora(){
        return $this->belongsTo('App\Montadora');
    }


    public function fotos()
    {
        return $this->hasMany('App\Foto');
    }

}