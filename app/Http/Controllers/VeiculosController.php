<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Montadora;
use App\Veiculo;
use Illuminate\Http\Request;

class VeiculosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $veiculos = Veiculo::all();
        return view('veiculos.index')->with('veiculos', $veiculos);
    }

    public function create()
    {
        $montadoras = Montadora::all();
        return view('veiculos.create')->with('montadoras', $montadoras);
    }

    public function store(Request $request)
    {
        if (isset($request)) {
            $veiculo = new Veiculo;
            $veiculo->nome = $request->nome;
            $veiculo->modelo = $request->modelo;
            $veiculo->preco = $request->preco;
            $veiculo->preco_fipe = $request->preco_fipe;
            $veiculo->montadora_id = $request->montadora;
            $veiculo->ativo = 1;

            $veiculo->descricao = $request->descricao;
            $veiculo->save();

            $fotos = $request->file('fotos');

            if ($request->hasFile('fotos')) {
                foreach ($fotos as $foto) {
                    $path = $foto->store('fotos', 'public');
                    $fotoObj = new Foto;
                    $fotoObj->veiculo_id = $veiculo->id;
                    $fotoObj->foto = $path;
                    $fotoObj->save();    
                }
            }

            return redirect()->route('veiculos.index')->with('message', 'Veiculo cadastrado com sucesso!!!!!');
        } else {
            return redirect()->route('veiculos.index');
        }

    }

    public function edit($id)
    {
        //
                
        if (isset($id)) {
            $veiculo = Veiculo::find($id);
            return view('veiculos.create', ['veiculo' => $veiculo, 'montadoras' => Montadora::all()]);
        }

    }

    public function update(Request $request, $id)
    {

        if (isset($request) && isset($id)) {
            $veiculo = Veiculo::find($id);            
            $veiculo->nome = $request->nome;
            $veiculo->modelo = $request->modelo;
            $veiculo->preco = $request->preco;
            $veiculo->preco_fipe = $request->preco_fipe;
            $veiculo->montadora_id = $request->montadora;
            $veiculo->ativo = 1;

            $veiculo->descricao = $request->descricao;
            $veiculo->save();

            $fotos = $request->file('fotos');

            if ($request->hasFile('fotos')) {
                foreach ($fotos as $foto) {
                    $path = $foto->store('fotos', 'public');
                    $fotoObj = new Foto;
                    $fotoObj->veiculo_id = $veiculo->id;
                    $fotoObj->foto = $path;
                    $fotoObj->save();    
                }
            }
        }
        
        return redirect()->route('veiculos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Veiculo::destroy($id);
        return redirect()->route('veiculos.index');
    }


    public function destroy_foto(Request $request){
        //echo "id = ".$request->campo_id;
        $res = Foto::destroy($request->campo_id);
        echo "ok foto destruida";
        return redirect()->route('veiculos.index');
    }

}
