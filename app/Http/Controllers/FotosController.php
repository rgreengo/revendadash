<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Veiculo;
use Illuminate\Http\Request;

class FotosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        $fotos = Foto::paginate(20);
        return view('fotos.index', compact('fotos'));
    }

}
