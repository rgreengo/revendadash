<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    //
    protected $fillable = [
        'foto',
    ];

    protected $guarded = ['id', 'created_at', 'update_at'];

    function veiculo(){
        return $this->belongsTo('App\Veiculo');
    }

}

