@extends('layouts.dash')

@section('content')
<div class="col-xl-12 col-lg-12">
    <div class="card shadow mb-4">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif   
    </div>
</div>
@endsection
