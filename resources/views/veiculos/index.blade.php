@extends('layouts.dash')

@section('content')
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="card-body">
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Veículos</h1>
    <a href="{{ url('veiculos/create') }}" class="btn btn-success btn-icon-split">
      <span class="icon text-white-50">
        <i class="fa fa-plus-circle"></i>
      </span>
      <span class="text">Inserir novo veiculo</span>
    </a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Montadora</th>
            <th>Valor</th>
            <th>Ativo</th>
            <th style="width: 100px;">Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($veiculos as $v)
          <tr>
            <td>{{ $v->id }}</td>
            <td>{{ $v->nome }}</td>
            <td>{{ $v->montadora->nome }}</td>
            <td>{{ number_format($v->preco,0,",",".") }}</td>
            <td>
              @if($v->ativo == 1)
              <span class="badge badge-pill badge-success">Ativo</span>
              @else
              <span>Inativo</span>
              @endif
            </td>
            <td style="text-align: center;">
              <a href="{{ route('veiculos.delete', $v->id) }}" class="jquery-postback btn btn-danger btn-circle btn-sm">
                <i class="fas fa-trash"></i>
              </a>

              <a href="veiculos/{{ $v->id }}/edit" class="btn btn-primary btn-circle btn-sm">
                <i class="fas fa-edit"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection