@extends('layouts.dash')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Veiculo</h1>
    <script src="{{ asset('js/jquery.js') }}"></script>
    
    <a href="{{ url('veiculos') }}" class="btn btn-light btn-icon-split">
        <span class="icon text-gray-600">
            <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text">Voltar</span>
    </a>
    <div class="card-body" style="width: 90%;">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

            @if(isset($veiculo))
            {{-- {{ Form::open(array('url' => 'veiculos', 'method' => 'put', $veiculo->id , 'files' => true)) }} --}}
                {{ Form::open(array('method'=>'PUT', 'files' => true, 'route' => ['veiculos.update', $veiculo->id])) }}    
            @else
                <form role="form" method="POST" action="{{ url('veiculos/') }}" enctype="multipart/form-data">
             @endif
                    {{ csrf_field() }}
                    <meta name="_token" content="{{ csrf_token() }}">
                    <div class="form-row">
                        <label for="nome" class="col-sm-12 col-form-label">Nome:</label>
                        <input class="form-control" type="text" name="nome" id="nome"
                            @if(isset($veiculo))value="{{$veiculo->nome}}" @endif>
                    </div>
                    <div class="form-row">
                        <label for="modelo" class="col-sm-12 col-form-label">Modelo:</label>
                        <input class="form-control" type="text" name="modelo" id="modelo"
                            @if(isset($veiculo))value="{{$veiculo->modelo}}" @endif>
                    </div>
                    <div class="form-row">
                        <label for="preco" class="col-sm-12 col-form-label">Preço:</label>
                        <input class="form-control" type="text" name="preco" id="preco"
                            @if(isset($veiculo))value="{{$veiculo->preco}}" @endif>
                    </div>
                    <div class="form-row">
                        <label for="preco_fipe" class="col-sm-4 col-form-label">Valor FIPE:</label>
                        <input class="form-control" type="text" name="preco_fipe" id="preco_fipe"
                            @if(isset($veiculo))value="{{$veiculo->preco_fipe}}" @endif>
                    </div>
                    <div class="form-row">
                        <label for="montadora" class="col-sm-2 col-form-label">Montadora:</label>
                        <select class="form-control" name="montadora" id="montadora">
                            @foreach($montadoras as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->nome }}</option>
                            @endforeach
                        </select>
                    </div>

                    @if(isset($veiculo))                    
                    <script>
                        $(document).ready(function() {
                            //console.log( "ready!" );
                            $('#montadora').val('{{ $veiculo->montadora_id }}')
                        });
                    </script>
                    @endif

                    <div class="form-row">
                        <label for="fotos" class="col-sm-4 col-form-label">Fotos:</label>
                        <input class="form-control-file" type="file" name="fotos[]" id="fotos" multiple>
                    </div>

                    <hr>
                    @if(isset($veiculo))
                    @foreach($veiculo->fotos as $ft)
                        <img src="../../storage/{{ $ft->foto }}" class="card-img" style="width: 150px; height: 100px;" />
                        <label class="col-sm-12 col-form-label" style="margin-right:10px; width:5%;">
                        <button type="button" class="close" aria-label="Close" id="{{ $ft->id }}">
                            <span aria-hidden="true" title="Excluir foto">&times;</span>
                          </button>
                        </label>
                        <hr>                    
                    @endforeach
                    <script>
                        $(document).ready(function() {
                            //
                            var _token = $('meta[name="_token"]').attr('content');
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': _token
                                }
                            });

                            $('.close').on( "click", function() { 
                                //console.log( "ready! id = " + $(this).attr('id'));
                                $(this).parent().prev().hide();
                                $(this).parent().next().hide();
                                $(this).parent().hide();
                                $.ajax({
                                    url: "{{ route('ajax.destroy_foto') }}",
                                    type: "POST",
                                    data: "campo_id="+$(this).attr('id'),
                                    dataType: "html"
                                
                                }).done(function(resposta) {
                                    console.log(resposta);                                    

                                }).fail(function(jqXHR, textStatus ) {
                                    console.log("Request failed: " + textStatus);
                                
                                }).always(function() {
                                    console.log("completou");
                                });
                            });
                        });
                    </script>
                    @endif

                    <div class="form-row">
                        <label for="descricao" class="col-sm-4 col-form-label">Descrição:</label>
                        <textarea class="form-control form-control-lg" name="descricao" id="descricao"
                            rows="14">@if(isset($veiculo)){{ $veiculo->descricao }}@endif</textarea>
                    </div>

                    <div class="form-row" style="margin-top: 20px;">
                        <a href="{{ url('veiculos') }}" class="btn btn-dark btn-icon-split">
                            <span class="icon text-gray-600">
                                <i class="fas fa-arrow-left"></i>
                            </span>
                            <span class="text">Cancelar</span>
                        </a>

                        <button style="margin-left: 10px;" class="btn btn-success btn-icon-split" type="submit">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">SALVAR</span>
                        </button>
                    </div>
                @if(isset($veiculo))
                {{ Form::close() }}
                @else
                    </form>
                @endif            
        </table>
    </div>
</div>
@endsection