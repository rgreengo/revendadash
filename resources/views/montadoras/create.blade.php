@extends('layouts.dash')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Montadoras</h1>     
        <a href="{{ url('montadoras') }}" class="btn btn-light btn-icon-split">
            <span class="icon text-gray-600">
              <i class="fas fa-arrow-left"></i>
            </span>
            <span class="text">Voltar</span>
        </a>
        <div class="card-body" style="width: 50%;">            
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <form method="POST" action="">
                    {{ csrf_field() }}
                    <div class="form-row">    
                        <label for="nome" class="col-sm-2 col-form-label">Nome:</label>                  
                        <input class="form-control form-control-lg" type="text" name="nome" id="nome" placeholder="">
                    </div>   
                    
                    <div class="form-row" style="margin-top: 20px;">
                        <a href="montadoras" class="btn btn-dark btn-icon-split">
                            <span class="icon text-gray-600">
                              <i class="fas fa-arrow-left"></i>
                            </span>
                            <span class="text">Cancelar</span>
                        </a>

                        <button style="margin-left: 10px;" class="btn btn-success btn-icon-split" type="submit">
                            <span class="icon text-white-50">
                              <i class="fas fa-save"></i>
                            </span>
                            <span class="text">SALVAR</span>
                        </button>                        
                    </div>
                </form>
              </table>
        </div>
    </div>    
@endsection