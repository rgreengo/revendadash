@extends('layouts.dash')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="card-body">
          <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Montadoras</h1>     
          <a href="montadoras_new" class="btn btn-success btn-icon-split">
            <span class="icon text-white-50">
              <i class="fa fa-plus-circle"></i>
            </span>
            <span class="text">Inserir nova montadora</span>
          </a>  
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nome</th>   
                    <th style="width: 100px;">Ações</th>                    
                  </tr>
                </thead>
                  @if (count($montadoras) >= 20)
                  <tfoot>
                    <tr>
                      <th colspan="4">{{ $montadoras->links() }}</th>
                    </tr>
                  </tfoot>
                  @endif
                  <tbody>                   
                    @foreach ($montadoras as $m)
                    <tr>
                      <td>{{ $m->id }}</td>
                      <td>{{ $m->nome }}</td>                      
                      <td style="text-align: center;">                        
                          <a href="" class="btn btn-danger btn-circle btn-sm">
                            <i class="fas fa-trash"></i>
                          </a>

                          <a href="montadoras/{{ $m->id }}/edit" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-edit"></i>
                          </a>                   
                      </td>
                    </tr>
                    @endforeach                    
                  </tbody>
              </table>
            </div>
        </div>
    </div>

@endsection
