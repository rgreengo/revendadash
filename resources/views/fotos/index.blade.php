@extends('layouts.dash')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="card-body">
          <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Fotos</h1>     
          <a href="novo_veiculo" class="btn btn-success btn-icon-split">
            <span class="icon text-white-50">
              <i class="fa fa-plus-circle"></i>
            </span>
            <span class="text">Inserir nova foto</span>
          </a>  
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nome</th>           
                    <th>Veiculo</th>                
                    <th>Data Cadastro</th>                    
                    <th style="width: 100px;">Ações</th>                    
                  </tr>
                </thead>
                @if (count($fotos) >= 20)
                  <tfoot>
                    <tr>
                      <th colspan="4">{{ $fotos->links() }}</th>
                    </tr>
                  </tfoot>
                  @endif
                  <tbody>                   
                    @foreach ($fotos as $f)
                    <tr>
                      <td>{{ $f->id }}</td>
                      <td>{{ $f->foto }}</td>
                      <td>{{ $f->veiculo->nome }}</td>
                      <td>{{ $f->created_at }}</td>
                      <td style="text-align: center;">                        
                          <a href="" class="btn btn-danger btn-circle btn-sm">
                            <i class="fas fa-trash"></i>
                          </a>

                          <a href="veiculos/{{ $f->id }}/edit" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-edit"></i>
                          </a>                   
                      </td>
                    </tr>
                    @endforeach                    
                  </tbody>
              </table>
            </div>
        </div>
    </div>

@endsection
