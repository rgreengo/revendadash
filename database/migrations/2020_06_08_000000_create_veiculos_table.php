<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVeiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('veiculos', function (Blueprint $table) {
        $table->id();
        $table->string('nome');
        $table->string('modelo');
        $table->decimal('preco', 8, 2);
        $table->decimal('preco_fipe', 8, 2);
        $table->boolean('ativo');
        $table->longText('descricao');
        
        $table->unsignedBigInteger('fabricante_id');
        $table->foreign('fabricante_id')->references('id')->on('fabricantes');
        $table->unsignedBigInteger('foto_id');
        $table->foreign('foto_id')->references('id')->on('fotos');

        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculos');
    }
}
