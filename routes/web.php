<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/home', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('veiculos', 'VeiculosController');
Route::get('veiculos/delete/{veiculo}', ['as' => 'veiculos.delete', 'uses' => 'VeiculosController@destroy']);
Route::post('destroy_foto', ['as' => 'ajax.destroy_foto', 'uses' => 'VeiculosController@destroy_foto']);

Route::resource('fotos', 'FotosController');
Route::resource('montadoras', 'MontadorasController');

